FROM node
MAINTAINER omicto
ENV HOME /root
RUN git clone https://gitlab.com/omicto/hw-container.git
WORKDIR hw-container
CMD node app.js